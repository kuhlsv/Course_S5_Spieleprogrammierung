﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MusicController : MonoBehaviour
{
    public AudioSource MusicGame;
    public AudioSource MusicMenue;
    private static MusicController instance = null;
    public static MusicController Instance
    {
        get { return instance; } // Music singleton
    }

    void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }

        DontDestroyOnLoad(this.gameObject); // Dont destroy
    }
}
