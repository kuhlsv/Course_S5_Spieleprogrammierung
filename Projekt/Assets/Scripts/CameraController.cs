﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    enum CamerMode
    {
        FPV, // First Person View
        TPV  // Thris Person View
    }

    public float horizontalSpeed = 100;
    public float rotationSpeed = 100;
    public bool rotationInverted = false;
    private readonly float rotationSensitivity = 3;
    private readonly float defaultFov = 60;
    public float maxFovIn = 15;
    public float maxFovOut = 15;
    public GameObject ball;
    private Vector3 followingOffset;
    private Quaternion startRotation;
    private CamerMode cameraMode = CamerMode.TPV;
    private bool canMouseY = false;

    void Start()
    {
        // Calculate the offset
        followingOffset = transform.position - ball.transform.position;
        // Get the default rotation for FPV
        startRotation = transform.localRotation;
    }

    /**
     * Handle active actions
     * Zoom in/out and change camera perspective
     **/
    void Update()
    {
        /*
         * Zoom is handled by changing FOV. This allows to expand the field of view to handle
         * a bigger amount of input of the map. This make a feeling like zoom in/out.
         */
        // Zoom
        float fov = Camera.main.fieldOfView;
        if (Input.GetAxis("Mouse ScrollWheel") > 0f)
        {
            // Up
            if (fov >= defaultFov - maxFovOut)
            {
                Camera.main.fieldOfView += -2;
                Camera.main.gameObject.transform.Rotate(1, 0, 0);
            }
        }
        else if (Input.GetAxis("Mouse ScrollWheel") < 0f)
        {
            // Down
            if (fov <= defaultFov + maxFovOut)
            {
                Camera.main.fieldOfView += 2;
                Camera.main.gameObject.transform.Rotate(-1, 0, 0);
            }
        }
        // Swtich Camera perspective 
        if (Input.GetKeyDown(KeyCode.Q))
        {
            if(cameraMode == CamerMode.FPV)
            {
                cameraMode = CamerMode.TPV;
            }
            else
            {
                cameraMode = CamerMode.FPV;
            }
        }
        // Activate Y Camera perspective 
        if (Input.GetKeyDown(KeyCode.E) && cameraMode == CamerMode.TPV)
        {
            canMouseY = !canMouseY;
        }
    }

    /**
     * Follow the Player and set rotation for the camera
     **/
    void LateUpdate() {
        // Get rotation from the mouse for the camera around player
        Quaternion mouseRotationX = Quaternion.AngleAxis(Input.GetAxis("Mouse X") * rotationSensitivity, Vector3.up);
        Quaternion mouseRotationY = Quaternion.AngleAxis(Input.GetAxis("Mouse Y") * rotationSensitivity, Vector3.left);
        if (canMouseY)
        {
            mouseRotationX = mouseRotationX * mouseRotationY;
        }
        // Set values to following the ball
        if (ball)
        {
            if(cameraMode == CamerMode.TPV)
            {
                // Rotation multy. to the offset
                followingOffset = mouseRotationX * followingOffset;
                // Set new position + offset with rotation
                transform.position = ball.transform.position + followingOffset;
                transform.LookAt(ball.transform.position);
            }
            else
            {
                // Different rotaion calc for FPV
                transform.localRotation = mouseRotationX * transform.localRotation;
                // Upset on y
                Vector3 upseted = ball.transform.position;
                upseted.y += 0.2f;
                // Set new position
                transform.position = upseted;
            }
        }
    }
}
