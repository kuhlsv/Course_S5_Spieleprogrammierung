﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnobController : MonoBehaviour
{
    public GameObject door1;
    public GameObject door2;
    public int yPressed;
    public int xPressed;
    public int zPressed;
    private bool pressed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!pressed)
        {
            pressed = true;
            Vector3 position = this.gameObject.transform.position;
            position.y += yPressed;
            position.x += xPressed;
            position.z += zPressed;
            this.gameObject.transform.position.Set(position.x, position.y, position.z);
            door1.gameObject.transform.Rotate(75, 0, 0);
            door2.gameObject.transform.Rotate(-75, 0, 0);

        }
    }
}
