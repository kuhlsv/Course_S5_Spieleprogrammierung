﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WeaponController : MonoBehaviour
{
    public bool destroyOnContact;
    public int damage;
    private GameObject weapon;

    private void Start()
    {
        weapon = transform.GetChild(0).gameObject;
        transform.Rotate(0, 90, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (destroyOnContact && collision.gameObject.CompareTag("Ball"))
        {
            transform.gameObject.SetActive(false);
        }
    }

    public int GetDamageValue()
    {
        return damage;
    }

    public void Hit()
    {
        if (weapon)
        {
            Vector3 rotate = new Vector3(0, 0, -90);
            weapon.transform.Rotate(rotate);
            Invoke("resetWeapon", 0.2f);
        }
    }

    void resetWeapon()
    {
        Vector3 rotate = new Vector3(0, 0, 90);
        weapon.transform.Rotate(rotate);
    }
}
