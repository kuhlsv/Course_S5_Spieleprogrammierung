﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectableScript : MonoBehaviour
{
    private readonly int ROTATION_SPEED = 120;
    private GameManager gameManager;
    int DAMAGE_ADD = 10;
    int SCORE_ADD = 20;

    private void Awake()
    {
        GameObject gm = GameObject.Find("GameManager");
        if (gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        // Spin
        transform.Rotate(0, ROTATION_SPEED * Time.deltaTime, 0);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "Player")
        {
            gameManager.AddDamage(DAMAGE_ADD);
            gameManager.AddScore(SCORE_ADD);
            Destroy(this.gameObject);
        }
    }
}
