﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonController : MonoBehaviour
{
    public Transform player;
    private Rigidbody rigidbodyComp;
    private Vector3 fixedPostition;
    private float elapsed = 0f;
    public GameObject ammo;
    private Transform hole;
    public float throwForce = 100;

    void Awake()
    {
        rigidbodyComp = GetComponent<Rigidbody>();
        fixedPostition = rigidbodyComp.position;
        hole = transform.GetChild(2); // Hole
    }

    // Update is called once per frame
    void LateUpdate()
    {
        elapsed += Time.deltaTime;
        if (player)
        {
            // Fix postition
            Vector3 playerPos = player.position;
            rigidbodyComp.transform.position = fixedPostition;

            // Check with Linecast if something is in the way or it can look/shoot to the player
            Debug.DrawLine(transform.position, player.transform.position, Color.red);
            if (Physics.Raycast(transform.position, player.transform.position))
            {
                // Shoot sequence
                if (elapsed >= 4f)
                {
                    elapsed = elapsed % 1f;
                    shoot(); // Shoot
                }
            }
            // Rotate to Player
            rigidbodyComp.transform.LookAt(player);
        }
    }

    void shoot()
    {
        GameObject ball = Instantiate(ammo, hole.position, Quaternion.identity);
        CannonballController cbc = ball.GetComponent<CannonballController>();
        cbc.Throw(transform.forward * throwForce);
    }
}
