﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Playables;

public class GameManager : MonoBehaviour 
{
    // Proberties
    private readonly int DEFAULT_LIFES = 3;
    private readonly string SCENE_END = "End";
    private readonly string SCENE_START = "Start";
    private readonly string SCENE_SETTINGS = "Settings";
    private readonly int LAST_LEVEL = 3; // Level count
    private static bool gameEnded = false;
    private static bool watchedIntro = false;
    private static int currentLevel = 0;
    private static int highscore;
    GameObject eHudFin; // Extended HUD for lvl finished
    GameObject eHudDied; // Extended HUD if died
    public FieldController lifesField;
    public FieldController scoreField;
    public FieldController damageField;
    public PlayableDirector intro;
    public MusicController musicManager;
    // Player stats
    public GameObject player;
    private static int lifes = 0;
    private static int score = 0;
    private static int damage = 0;
    private static bool foundWeapon = false;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        // Init HUD
        eHudFin = GameObject.Find("HUD/LevelFinished");
        eHudDied = GameObject.Find("HUD/Died");
        if(currentLevel == 1 || currentLevel == -1)
        {
            // Init Intro 
            if (GameObject.Find("IntroTimeline") && MusicController.Instance)
            {
                MusicController.Instance.MusicMenue.Stop();
                intro = GameObject.Find("IntroTimeline").GetComponent<PlayableDirector>();
                intro.Pause();
                intro.stopped += MusicPlay;
                if (currentLevel == -1) // From settings
                {
                    // If "Watch Intro again" selected, go back to Settings when finnished
                    intro.stopped += LoadSettings;
                }
            }
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (GetLifes() == 0)
        {
            ResetLifes();
        }
        // HUD init
        UpdateHud();
        if (eHudFin && eHudDied)
        {
            eHudFin.SetActive(false);
            eHudDied.SetActive(false);
        }
        // Start Intro
        if ((currentLevel == 1 && !watchedIntro) || currentLevel == -1)
        {
            Debug.Log("Play intro animation");
            MusicController.Instance.MusicGame.Stop();
            intro.Play();
            watchedIntro = true;
        }
    }

    private void FixedUpdate()
    {
        // HUD updates
        UpdateHud();
    }

    /*
     * Music
     * 
     * */

    private void MusicPlay(PlayableDirector a)
    { // Delegate
        if (MusicController.Instance)
        {
            MusicController.Instance.MusicGame.Play();
            // Fix player fall after sequence
            if (player)
            {
                Rigidbody rig = player.GetComponent<Rigidbody>();
                rig.velocity = Vector3.zero;
                rig.transform.rotation = Quaternion.LookRotation(rig.velocity);
            }
        }
    }

    /*
     * States
     * 
     * */

    public static int GetCurrentLevel()
    {
        return currentLevel;
    }

    public static bool HasGameEnded()
    {
        return gameEnded;
    }

    public void Died()
    {
        // Unlock mouse to click buttons
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Debug.Log("You died");
        ReduceLife();
        if(GetLifes() == 0)
        {
            LoadEnd();
        }
        else
        {
            // Show extended HUD to load next level
            if (eHudDied)
            {
                eHudDied.SetActive(true);
                return;
            }
        }
    }

    public void FinishedLevel()
    {   
        // Unlock mouse to click buttons
        Cursor.lockState = CursorLockMode.None;
        Cursor.visible = true;
        Debug.Log("Level finished");
        // Last level reached
        if (currentLevel == LAST_LEVEL)
        {
            // Show end
            GameManager.gameEnded = true;
            LoadEnd();
            // Check highscore
            if (GetScore() > highscore)
            {
                highscore = GetScore();
            }
        }
        else
        {
            // Not last level
            // Show extended HUD to load next level
            if (eHudFin)
            {
                eHudFin.SetActive(true);
            }
        }
    }

    private void UpdateHud()
    {
        // Set stats to HUD
        if (lifesField)
        {
            lifesField.SetValue(GetLifes());
        }
        if (scoreField)
        {
            scoreField.SetValue(GetScore());
        }
        if (damageField)
        {
            damageField.SetValue(GetDamage());
        }
    }

    /*
     * Scene control
     * 
     * */

    private void Load(string scene)
    {
        if(scene != SCENE_END && scene != SCENE_SETTINGS && scene != SCENE_START)
        {
            // Lock mouse ingame into screen
            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else
        {
            // Unlock mouse else to click buttons
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }

        // Load
        SceneManager.LoadScene(scene);        
        // Music
        /* For Debugging -> if (MusicController.Instance && !MusicController.Instance.MusicMenue.isPlaying) 
        {
            //MusicController.Instance.MusicGame.Stop();
            //MusicController.Instance.MusicMenue.Play();
        }*/
    }

    public void LoadEnd()
    {
        Load(SCENE_END);
    }

    public void LoadStart()
    {
        // Reset
        gameEnded = false;
        currentLevel = 0;
        ResetStats();
        // Load
        Load(SCENE_START);
    }

    public void LoadSettings()
    {
        Load(SCENE_SETTINGS);
    }

    public void LoadSettings(PlayableDirector a)
    {
        intro.stopped -= LoadSettings;
        watchedIntro = false;
        currentLevel = 0;
        LoadSettings();
    }

    public void LoadLevel(int number)
    {
        if(number == 0)
        {
            LoadStart();
            return;
        }
        // Music
        if (MusicController.Instance)
        {
            if (!MusicController.Instance.MusicGame.isPlaying && !watchedIntro)
            {
                MusicController.Instance.MusicMenue.Stop();
                Invoke("MusicPlay", 3);
            }
            if (MusicController.Instance.MusicMenue.isPlaying && number == 1 && !watchedIntro)
            {
                MusicController.Instance.MusicMenue.Stop();
            }

        }
        // Load
        if (currentLevel != -1)
        {
            currentLevel = number;
        }
        Load("Level" + number.ToString());
        Debug.Log("Start Level " + number);
    }

    public void LoadNextLevel()
    {
        if (currentLevel == LAST_LEVEL)
        {
            LoadEnd();
            return;
        }
        LoadLevel(currentLevel + 1);
    }

    public void ResetLevel()
    {
        LoadLevel(currentLevel);
    }

    public void ReplayIntro()
    {
        watchedIntro = false;
        currentLevel = -1; // To force quit after intro
        LoadLevel(1);
    }

    /*
     * Player stats
     * 
     * */

    public int GetLifes()
    {
        return lifes;
    }

    public void ReduceLife()
    {
        ReduceScore(50);
        lifes--;
    }

    public void ResetLifes()
    {
        lifes = DEFAULT_LIFES;
    }

    public void AddLife()
    {
        lifes++;
    }

    public int GetScore()
    {
        return score;
    }

    public void ReduceScore(int value)
    {
        score -= value;
    }

    public void AddScore(int value)
    {
        score += value;
    }

    public void ResetScore()
    {
        score = 0;
    }

    public bool FoundWeapon()
    {
        return foundWeapon;
    }

    public void FoundWeapon(bool state)
    {
        foundWeapon = state;
    }


    public void ResetWeapon()
    {
        foundWeapon = false;
    }

    public int GetDamage()
    {
        return damage;
    }

    public void AddDamage(int value)
    {
        damage += value;
    }

    public void ResetDamage()
    {
        damage = 0;
    }

    public void ResetStats()
    {
        ResetDamage();
        ResetLifes();
        ResetScore();
        ResetWeapon();
    }

    /*
     * Application control
     * 
     * */
    public void ExitGame()
    {
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
