﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerrainController : MonoBehaviour
{
    GameManager gameManager;
    public bool resetGameOnContact;
    public AudioSource sound;

    /// <summary>
    /// Awake is called when the script instance is being loaded.
    /// </summary>
    void Awake()
    {
        GameObject gm = GameObject.Find("GameManager");
        if(gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
    }

    private void OnCollisionEnter(Collision col)
    {
        if (resetGameOnContact && col.gameObject.CompareTag("Ball")){
            // Play sound
            if (sound)
            {
                sound.Play();
            }
            // Kill player
            Debug.Log("You Lost");
            Destroy(col.gameObject, 1);
            Invoke("Reset", 1); // Reset game after 1 seconds
        }
    }

    private void OnCollisionStay(Collision col)
    {
        if (col.gameObject.CompareTag("Ball") && gameObject.CompareTag("SlowGrass"))
        {
            BallController ball = col.gameObject.GetComponent<BallController>();
            ball.GetComponent<Rigidbody>().drag = 2.5f;
        }
    }

    private void OnCollisionExit(Collision col)
    {
        if (col.gameObject.CompareTag("Ball") && gameObject.CompareTag("SlowGrass"))
        {
            BallController ball = col.gameObject.GetComponent<BallController>();
            ball.GetComponent<Rigidbody>().drag = 0;
        }
    }

    private void Reset()
    {
        gameManager.Died();
    }
}
