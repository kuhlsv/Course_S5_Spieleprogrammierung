﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    private readonly float fallYRespawn = -10f;
    private readonly int WALLJUMP_FACTOR = 300;
    private readonly int MAX_WALLJUMP = 400;
    private Rigidbody rigidbodyComp;
    private Transform cameraTransformComp;
    private bool isOnGround;
    private Vector3 jumpForce;
    public float speed = 400f;
    public float jump = 400f;
    private Vector3 spawnPosition;

    void Awake()
    {
        rigidbodyComp = GetComponent<Rigidbody>();
        GameObject camera = GameObject.Find("Main Camera"); // Slow
        cameraTransformComp = camera.transform;
        jumpForce = new Vector3();
        // Stop ball to roll for debugging weapon
        //rigidbodyComp.freezeRotation = true;
    }

    void Update()
    {
        // Calculate movement force every frame with keypress
        // This adds also the jump-calculation force
        processMovement();
        // Calulate the jump-force in late frame
        processJump();
    }

    private void OnCollisionEnter(Collision collision)
    {
        string name = collision.gameObject.name;
        if (name.Contains("Plateau") || name.Contains("Platform") || name.Contains("Terrain")) { 
            // Is on ground
            isOnGround = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        // Is now not on ground
        isOnGround = false;
    }

    void OnCollisionStay(Collision obj)
    {
        // WALLJUMP: Check collidate a jumpable wall
        if (obj.gameObject.CompareTag("Jumpable"))
        {
            // Detect wall jump and calculate the bounce force
            Vector3 newVector = obj.GetContact(0).point - rigidbodyComp.position;
            newVector.z *= -5;
            newVector.x *= -3;
            if(newVector.y < 0){
                newVector.y *= -3;
            }
            jumpForce = newVector * WALLJUMP_FACTOR;
        }
        isOnGround = true;
    }

    void processJump()
    {
        /**
         * Jump
         * */
        if (Input.GetKeyDown("space") && isOnGround)
        {
            jumpForce.y *= (70f * jump * Time.deltaTime);
            if (jumpForce.y > MAX_WALLJUMP)
            {
                jumpForce.y = MAX_WALLJUMP;
            }
            rigidbodyComp.AddForce(jumpForce.x, jumpForce.y, jumpForce.z); // Add
            isOnGround = false;
        }
        jumpForce = Vector3.one; // Reset
    }

    void processMovement()
    {
        /**
        * Movement
        * */
        float speedX = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float speedZ = Input.GetAxis("Vertical") * speed * Time.deltaTime;
        if (Input.GetKey(KeyCode.LeftShift) && Input.GetKey(KeyCode.W))
        { // Run
            speedX *= 2f;
            speedZ *= 2f;
            jumpForce.y *= 1.5f;
        }
        // Add force to roll
        // Roll to Camera direktion
        Vector3 direktionForce = Camera.main.transform.TransformDirection(speedX, 0, speedZ);
        if (direktionForce != Vector3.zero)
        {
            // Add force to roll
            rigidbodyComp.AddForce(direktionForce * 3);
        }

        // Prevent fall off the map, it will reset the player
        if (rigidbodyComp.position.y < fallYRespawn)
        {
            rigidbodyComp.position = spawnPosition;
        }
    }
}
