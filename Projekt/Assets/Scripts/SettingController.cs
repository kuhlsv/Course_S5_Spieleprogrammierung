﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingController : MonoBehaviour
{
    public Slider VolumeSliderGame;
    public Slider VolumeSliderMenue;

    // Start is called before the first frame update
    void Start()
    {        
        // Set slider to current volume
        if (VolumeSliderGame && VolumeSliderMenue && MusicController.Instance)
        {
            // Set global volume to current 
            VolumeSliderGame.value = MusicController.Instance.MusicGame.volume;
            VolumeSliderMenue.value = MusicController.Instance.MusicMenue.volume;
        }

    }

    // Update is called once per frame
    void Update()
    {
        // When settings are modified
        if (VolumeSliderGame && VolumeSliderMenue && MusicController.Instance)
        {
            // Set global volume to current 
            MusicController.Instance.MusicGame.volume = VolumeSliderGame.value;
            MusicController.Instance.MusicMenue.volume = VolumeSliderMenue.value;
        }
    }
}
