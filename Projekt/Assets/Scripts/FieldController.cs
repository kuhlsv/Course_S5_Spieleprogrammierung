﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FieldController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        if(gameObject.name == "EndInfo")
        {
            if (GameManager.HasGameEnded())
            {
                SetValue("YOU WON!");
            }
            else
            {
                SetValue("YOU LOST!");
            }
        }
    }

    public void SetValue(int value){
        GetComponent<Text>().text = value.ToString();
        // Durty weil man hier GetComponent aufruft und nicht in awake und als attribut
    }

    public void SetValue(string text)
    {
        GetComponent<Text>().text = text;
        // Durty weil man hier GetComponent aufruft und nicht in awake und als attribut
    }
}