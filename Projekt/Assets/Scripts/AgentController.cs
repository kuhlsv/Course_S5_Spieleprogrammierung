﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AgentController : MonoBehaviour
{
    public Transform player;
    private GameManager gameManager;
    public GameObject enemyHealthBar;
    private Vector2 healthBarStartSize;
    private readonly int START_HEALTH = 1000;
    private float health;
    private int hits = 0;

    private void Awake()
    {
        GameObject gm = GameObject.Find("GameManager");
        if (gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
        // Get Healbar size
        health = START_HEALTH;
        healthBarStartSize = enemyHealthBar.GetComponent<RectTransform>().sizeDelta;
    }

    // Update is called once per frame
    void Update()
    {
        // Follow player
        if (player)
        {
            // Lookup where player is and walk there -> Not cleanest. TODO: Just update, if something changed.
            GetComponent<NavMeshAgent>().destination = player.position;
        }
        // Check dead
        if(health <= 0)
        {
            Died();
        }
        // Update health bar by setting the new width in reference to health
        enemyHealthBar.GetComponent<RectTransform>().SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, healthBarStartSize.x * (health/START_HEALTH));
    }

    private void OnCollisionEnter(Collision collision)
    {
        // Kill player
        if (collision.gameObject.CompareTag("Ball"))
        {
            hits += 1;
            if(hits > 4)
            {
                gameManager.Died();
                Destroy(this.gameObject);
            }
        }
        // Get Damage
        if (collision.gameObject.CompareTag("Weapon"))
        {
            try
            {
                this.health -= gameManager.GetDamage();
            }
            catch(Exception e)
            {
                Debug.Log("Error: Cant damage enemy." + e.Message);
            }
        }
    }

    private void Died()
    {
        GameObject jail = GameObject.Find("Jail");
        if (jail)
        {
            jail.SetActive(false);
        }
        Debug.DrawLine(transform.position, jail.transform.position);
        Destroy(this.gameObject);
    }
}
