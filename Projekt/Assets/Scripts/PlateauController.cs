﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlateauController : MonoBehaviour
{
    private GameManager gameManager;

    // Start is called before the first frame update
    void Awake()
    {
        GameObject gm = GameObject.Find("GameManager");
        if (gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
        GetComponentInChildren<FieldController>().SetValue(gameObject.tag);
    }

    private void OnCollisionEnter(Collision col)
    {
        // Detect if platform is an end und ball entered
        foreach (ContactPoint cp in col.contacts)
        {
            if (cp.thisCollider.name == "Platform" && col.gameObject.CompareTag("Ball") && gameObject.CompareTag("End"))
            {
                gameManager.FinishedLevel();
                col.gameObject.SetActive(false);
            }
        }
    }
}
