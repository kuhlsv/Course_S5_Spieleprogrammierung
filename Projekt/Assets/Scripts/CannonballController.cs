﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonballController : MonoBehaviour
{
    Rigidbody rigidbodyComp;
    private GameManager gameManager;

    // Start is called before the first frame update
    void Awake()
    {
        rigidbodyComp = GetComponent<Rigidbody>();
        GameObject gm = GameObject.Find("GameManager");
        if (gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
    }

    public void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Ground" || collision.gameObject.name == "Bridge")
        {
            Destroy(this.gameObject);
        }
        if(collision.gameObject.name == "Player")
        {
            gameManager.Died();
        }
    }

    public void Throw(Vector3 throwVector)
    {
        // Debug.Log("Shoot");
        rigidbodyComp.AddForce(throwVector);
    }
}
