﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
 * Player has to have a "Ball" tag
 */

public class PlayerController : MonoBehaviour
{
    public Vector3 WEAPON_OFFSET = new Vector3(0.02f, 0.01f, 0.02f);
    public bool weaponEnabled = false;
    public WeaponController weapon;
    // Game Manager
    private GameManager gameManager;

    private void Awake()
    {
        GameObject gm = GameObject.Find("GameManager");
        if (gm)
        {
            this.gameManager = gm.GetComponent<GameManager>();
        }
    }

    private void Start()
    {
        // Check weapon found
        if (gameManager.FoundWeapon())
        {
            weaponEnabled = true;
        }
        // Enable weapon dynamicly
        if (weaponEnabled)
        {
            SetWeapon(weaponEnabled);
        }
    }

    private void Update()
    {
        // Inform Weapon about position to follow
        UpdateWeaponPosition();
        // Hit with weapon
        if (Input.GetMouseButtonDown(0))
        {
            Hit();
        }
    }

    void OnCollisionEnter(Collision collision)
    {
        // Player can take/pull a Weapon
        if (!weaponEnabled)
        {
            if(collision.gameObject.CompareTag("CollectableWeapon")){
                SetWeapon(true);
                weaponEnabled = true;
                // Add damage from weapon to player
                int wpDmg = 0;
                try
                {
                    WeaponController wc = collision.gameObject.GetComponent<WeaponController>();
                    if (wc)
                    {
                        // Get weapon damage
                        wpDmg = wc.GetDamageValue();
                    }
                }
                catch (Exception e)
                {
                    Debug.Log(e.Message);
                }
                gameManager.AddDamage(wpDmg);
                gameManager.AddScore(wpDmg * 2);
                gameManager.FoundWeapon(true);
            }
        }

        // Check this is princess and player got me
        if(GameManager.GetCurrentLevel() > 1 && this.gameObject.name == "Princess" && collision.gameObject.name == "Player")
        {
            gameManager.FinishedLevel();
        }
    }

    void SetWeapon(bool value)
    {
        // Set first childs state
        if (weapon && gameObject.transform.childCount > 0)
        {
            weapon.gameObject.SetActive(value);
        }
    }

    void UpdateWeaponPosition()
    {
        // Set the position of the weapon to this + a offset and let him rotate like the player
        if (weapon && weaponEnabled)
        {
            Vector3 newPosition = transform.position + WEAPON_OFFSET;
            weapon.transform.LookAt(Camera.main.transform.position);
            weapon.transform.position = newPosition;
        }
    }

    /*
     * Actions
     * 
     * */

    void Hit()
    {
        // Give weapon signal to hit
        if (weapon)
        {
            weapon.Hit();
        }
    }
}
