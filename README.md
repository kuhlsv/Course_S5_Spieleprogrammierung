<b>Version</b>: Unity 2018.3.14f1<br>
<b>Name</b>: Sven Kuhlmann<br>
<b>Email</b>: Sven.Kuhlmann@stud.hs-flensburg.de<br>
<b>MatrNr</b>: *****<br>
<br>
Größe des Projectes (nur das finale Master project gezipped): 105MB<br>
Das Projekt verwendet nur Assets aus dem Asset-Store. Quellen dazu sind im Ordner Dokumente anzufinden.<br>
<br>
<b>Das Spiel:</b><br>
Das Spiel dartet bei ersten Start mit einem Intro bei der dem Spieler seine Prinzessin von einem Gegner enführt wird.<br>
Anschließend kann man als Kugel durch das Level Rollen und unter anderem ein Schwert oder Äpfel, die einen Stärken, einsammeln.
Das Schwert wird für den Endkampf benötigt und "folgt" dem Spieler.<br>
Der Spieler stirbt wenn er von einer Platform fällt, Wasser berührt oder zu Oft vom Gegner berührt wird.<br>
Ist der Endkampf gewonnen, so verschwinden die Gefängnisgitter in der Burg und man kann die Prinzessin berühren, um das Spiel zu gewinnen.<br>